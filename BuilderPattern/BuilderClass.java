class Form
{
	final private int empid;
	final private String fname;
	final private String lname;
	final private String address;

	//constructor
	private Form(Dummy ob)  //to create the original form class object from the dummmy object
	{
		empid = ob.empid;
		fname = ob.fname;
		lname = ob.lname;
		address = ob.address;
	}
	//to access value we provide getter method but not setter method because value is emutable(can not be changed)
	public int getEmpId(){
		return empid;

	}

	public String getFname(){
		return fname;
	}

	public String getLname(){
		return lname;

	}

	public String getAddress(){
		return address;
	}

	//dummy class will be a static nested class

	public static class Dummy
	{
		//compulsery attribute will be final in dummy class and optional attribute will be normal
		private int empid;
		final private String fname;
		final private String lname;
		private String address;

		//constructor for all the compulsery attribute
		Dummy(String f, String l)
		{
			fname = f;
			lname = l;
		}
		//setter method will be provided to update all the optional input
		public Dummy setAddress(String ad){
			address = ad;
			return this; //return the object after updation because the setter method is of dummy type
		}

		public Dummy setEmpId(int emp){
			empid = emp;
			return this;
		}

		//a build mehtod will be available to return dummy object to the actual object
		public Form build()
		{
			return new Form(this); //creating object of Form class with Dummy class attributes(this object/current object)
		}
	}
}

class TestBuilder{
	public static void main(String[] args) 
	{
		Form f = new Form.Dummy("Chetan","Vagat").setEmpId(200).build(); //accessing dummy static nested class then creating object
		System.out.println(f.getEmpId());

		Form f1 = new Form.Dummy("Jiten","Dhar").build();
		System.out.println(f1.getEmpId());
		System.out.println(f1.getAddress());


		Form f2 = new Form.Dummy("Lokesh","Rana").setEmpId(100).setAddress("Lakhnow").setAddress("Delhi").build(); //laknow will be replaced with delhi but after build method is called it can't be changed
		System.out.println(f2.getAddress());
	}
}